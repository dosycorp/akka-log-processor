package com.dosaygo.cris.actorsystems.logprocessor.system;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import com.dosaygo.cris.actorsystems.logprocessor.actors.FileScanner;
import com.dosaygo.cris.actorsystems.logprocessor.messages.Scan;

public class Main {
  
  public static void main( String... args ) {

    // set up predefine log directory, or accept a command line argument
      final String DEFAULT_LOG_DIR = "../logs";
      String logDir = DEFAULT_LOG_DIR;

      if ( args.length > 0 )
        logDir = args[ 0 ];

    // create the actor system and the FileScanner
      ActorSystem system = ActorSystem.create( "log-processor" );
      ActorRef fileScannerActor = system.actorOf( Props.create( FileScanner.class, logDir ), "file-scanner" );

    // tell the file scanner to Scan
      fileScannerActor.tell( new Scan( ), null );

  }

}
