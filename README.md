# akka-log-processor

A very simple Actor System for asynchronous processing of logs using Akka

## compiling

`mvn compile`

## building an executable jar with dependencies

`mvn clean compile assembly:single`

## running without building a jar

`mvn exec:java -Dexec.mainClass = "com.dosaygo.cris.actorsystems.logprocessor.system.Main"`

