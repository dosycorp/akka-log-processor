package com.dosaygo.cris.actorsystems.logprocessor.actors;

/**
 * LineAggregator
 *
 * This class is an Actor 
 * that receives events to do with 
 * file parsing and reports
 * on them in some cases. 
 *
 * If an instance receives an EndOfFile event
 * it prints to the console the number of lines
 * in the file as specified in the EndOfFile event.
 *
 * For all other file parsing events, it only reports
 * that is has received the event.
 *
 * The decision was made to not handle the counting of lines
 * in the line aggregator class because of the following 
 * reasons:
 * - events can arrive out of order, or not at all, so in that case 
 * a maxLineNumber record must be kept, to note the largest lineNumber
 * we have recieved.
 * - if the Actor only has a maxLineNumber kept
 * and receives an EndOfFile event, out of order, and before it has
 * received other, larger, line numbers, then the Actor
 * will report an incorrect number of lines for the file.
 * - the solution to this is to wait for all Line events to arrive
 * but how do we know when they have? Only if we can compare them 
 * against a totalLineCount for the file. And the only source of truth
 * for that will be the place where the Line events are emitted from, and this
 * count will arrive in the EndOfFile event.
 * - because of this it is necessary for this Actor to not count 
 * the number of lines, because they will already be present in the EndOfFile event,
 * although this Actor could do other processing on each line.
 *
 * The Line Aggregator or subclasses could perform functions such as:
 * - tokenizing
 * - summary statistics accumulation
 * - complex event processing
 * 
 * And it works to keep these functions separate from the actual 
 * parsing of bytes and production of lines, so 
 * the LineAggregator seems like a workable 
 * design in a log processing 
 * Actor system.
 *
 **/

import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;

import java.nio.file.Path;

import com.dosaygo.cris.actorsystems.logprocessor.events.StartOfFile;
import com.dosaygo.cris.actorsystems.logprocessor.events.Line;
import com.dosaygo.cris.actorsystems.logprocessor.events.EndOfFile;

public class LineAggregator extends AbstractActor {

  protected final LoggingAdapter log = Logging.getLogger( context( ).system( ), this );
  protected final Path fileToAggregate;

  private LineAggregator( Path fileToAggregate ) {

    this.fileToAggregate = fileToAggregate;

    receive( ReceiveBuilder.
      
      match( StartOfFile.class, event -> {
        
        if ( this.fileToAggregate.equals( event.targetFile ) ) 
          log.info( "LineAggregator received StartOfFile event: {}", event );

      } ).
      match( Line.class, event -> {
        
        if ( this.fileToAggregate.equals( event.targetFile ) ) 
          log.info( "LineAggregator received Line event: {}", event );

      } ).
      match( EndOfFile.class, event -> this.handle( event ) ).
      matchAny( o -> log.info( "LineAggregator does not understand this event {}", o ) ).
      build( )

    );

  }

  private void handle( EndOfFile event ) {

    if ( this.fileToAggregate.equals( event.targetFile ) ) {
      log.info( "LineAggregator received EndOfFile event: {}", event );

      // Perform spec requirements
      this.outputLineCount( event );
    }

  }

  private void outputLineCount( EndOfFile event ) {

    if ( System.console( ) != null ) {
      System.console( ).writer( ).println( "File " + event.targetFile + " has " + event.finalLineNumber + " lines" );
      System.console( ).writer( ).flush( );
    } else {
      System.out.println( "File " + event.targetFile + " has " + event.finalLineNumber + " lines" );
      System.out.flush( );
    }

  }

}


