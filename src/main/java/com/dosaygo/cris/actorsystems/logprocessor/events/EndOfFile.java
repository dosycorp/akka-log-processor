package com.dosaygo.cris.actorsystems.logprocessor.events;

/**
 * ENDOFFILE
 * An Event
 * Dispatched
 * - from FileParser
 * - to Aggregator
 * - when FileParser reaches the end of a file.
 *
 * Specifies
 * - targetFile
 * - finalLineNumber
 **/

import java.nio.file.Path;

public class EndOfFile {

  public final Path targetFile;
  public final long finalLineNumber;

  public EndOfFile( Path targetFile, long finalLineNumber ) {

    this.targetFile = targetFile;
    this.finalLineNumber = finalLineNumber;

  }

  @Override
  public String toString( ) {

    return "EndOfFile{ targetFile = " + this.targetFile + ", finalLineNumber = " + this.finalLineNumber + " }";

  }

}

